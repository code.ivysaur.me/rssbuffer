# rssbuffer

![](https://img.shields.io/badge/written%20in-Golang-blue)

An RSS server that buffers messages from other RSS feeds.

Existing RSS/Atom servers supply a finite list of recent articles. A feed reader is expected to retrieve articles regularly enough so that no articles are missed. This project retrieves upstream articles regularly and buffers them in order to provide a much larger set of articles downstream.

## Usage

1. Launch the server application (e.g. `rssbuffer -BindTo ':8080'`)

2. Identify an upstream RSS feed (e.g. `example.com/rss.xml`)

3. Navigate to `localhost:8080/rss/http:/example.com/rss.xml`
- The upstream RSS feed will be fetched, parsed, cached and returned automatically.
- The upstream feed will be refreshed regularly in order to provide new articles.
- `rssbuffer` will provide up to `-HistoryDays` days of articles.

## Arguments


```
Usage of rssbuffer:
  -BackgroundRefreshMinutes int
        Interval between refreshing known feeds (default 30)
  -BindTo string
        Address/port to bind to (default "127.0.0.1:8123")
  -HistoryDays int
        Number of days' history to return in queries (default 7)
  -VerboseLog
        Verbose logging
```


## Changelog

2017-02-04 1.0.1
- Update RSS support library for improved compatibility
- Restructure repository, employ `gvt` for dependency management
- [⬇️ rssbuffer-1.0.1-win64.7z](dist-archive/rssbuffer-1.0.1-win64.7z) *(1.20 MiB)*
- [⬇️ rssbuffer-1.0.1-win32.7z](dist-archive/rssbuffer-1.0.1-win32.7z) *(1.11 MiB)*
- [⬇️ rssbuffer-1.0.1-src.zip](dist-archive/rssbuffer-1.0.1-src.zip) *(6.84 KiB)*
- [⬇️ rssbuffer-1.0.1-linux64.tar.xz](dist-archive/rssbuffer-1.0.1-linux64.tar.xz) *(1.32 MiB)*
- [⬇️ rssbuffer-1.0.1-linux32.tar.xz](dist-archive/rssbuffer-1.0.1-linux32.tar.xz) *(1.24 MiB)*


2016-05-01 1.0.0
- Initial public release
- [⬇️ rssbuffer-1.0.0-win64.7z](dist-archive/rssbuffer-1.0.0-win64.7z) *(1.41 MiB)*
- [⬇️ rssbuffer-1.0.0-win32.7z](dist-archive/rssbuffer-1.0.0-win32.7z) *(1.32 MiB)*
- [⬇️ rssbuffer-1.0.0-src.zip](dist-archive/rssbuffer-1.0.0-src.zip) *(4.38 KiB)*
- [⬇️ rssbuffer-1.0.0-linux64.tar.xz](dist-archive/rssbuffer-1.0.0-linux64.tar.xz) *(1.54 MiB)*
- [⬇️ rssbuffer-1.0.0-linux32.tar.xz](dist-archive/rssbuffer-1.0.0-linux32.tar.xz) *(1.44 MiB)*

